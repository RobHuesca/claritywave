# Claritywave challenge

Test para vacante Django Developer realizada por Roberto Huesca

## Instalación 

El proyecto está en un contenedor [docker](https://www.docker.com) por lo tanto para ejecutarlo se necesita primero correr el siguiente comando para crear la imagen

```bash
docker compose -f local.yml build
```
Una vez creada la imagen se debe ejecutar el contenedor con el siguiente comando
```bash
docker compose -f local.yml up
```
## Uso
Una vez ejecutado el contenedor podemos acceder a través de la direccion:
```bash
localhost:8000
Lista de usuarios:
- username: develop password:demo2021
- username: root password:demo2021
``` 
Para poder contar con datos necesitamos cargar el fixture que se encuentra dentro de la carpeta db ejecutando el comando:
```bash
docker compose -f local.yml run --rm django python manage.py loaddata db/db.json
``` 
## Tests
Para poder ejecutar los test realizados se debe ejecutar el siguiente comando:
```bash
docker compose -f local.yml run --rm django pytest
``` 
