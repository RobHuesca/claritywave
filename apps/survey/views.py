import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from apps.survey.models import Question, Answer


class QuestionListView(LoginRequiredMixin, TemplateView):
    template_name = 'survey/question_list.html'

    def get_context_data(self, **kwargs):
        context = super(QuestionListView, self).get_context_data(**kwargs)
        # Limitar solo mostrar 20 preguntas
        # context['questions'] = sorted(Question.objects.all()[:20], key=lambda question: -question.ranking())
        context['questions'] = sorted(Question.objects.all(), key=lambda question: -question.ranking())
        return context


class QuestionCreateView(LoginRequiredMixin, CreateView):
    model = Question
    fields = ['title', 'description']
    redirect_url = ''

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class QuestionUpdateView(LoginRequiredMixin, UpdateView):
    model = Question
    fields = ['title', 'description']
    template_name = 'survey/question_form.html'


def answer_question(request):
    data_response = json.loads(request.body)
    question_pk = data_response["question_pk"]
    if not question_pk:
        return JsonResponse({'ok': False})
    try:
        question = Question.objects.filter(pk=question_pk)[0]
        answer = Answer.objects.get(question=question, author=request.user)
        answer.value = data_response["value"]
        answer.save()
    except Answer.DoesNotExist:
        question = Answer.objects.create(question_id=question_pk, author=request.user, value=data_response["value"])

    return JsonResponse({'ok': True})


def like_dislike_question(request):
    data_response = json.loads(request.body)
    question_pk = data_response["question_pk"]
    if not question_pk:
        return JsonResponse({'ok': False})
    try:
        question = Question.objects.filter(pk=question_pk)[0]
        answer = Answer.objects.get(question=question, author=request.user)
        answer.like = data_response["value"]
        answer.save()
    except Answer.DoesNotExist:
        question = Answer.objects.create(question_id=question_pk, author=request.user, like=data_response["value"])
    return JsonResponse({'ok': True})

