from django.test import TestCase
from apps.survey.models import Question, Answer
from django.contrib.auth import get_user_model


class QuestionTestCase(TestCase):
    def setUp(self):
        # Users
        self.author = get_user_model().objects.create(first_name='Author', username='author')
        self.user_1 = get_user_model().objects.create(first_name='User', username='user_one')
        self.user_2 = get_user_model().objects.create(first_name='User', username='user_two')

        # Questions and aswers
        self.question = Question.objects.create(title="¿Te gusta desarrollar?", description="Pregunta 1", author=self.author)
        self.question_2 = Question.objects.create(title="¿Te gusta desarrollar en python?", description="Pregunta 2", author=self.author)

    def test_answer_ranking(self):
        question = Question.objects.get(description="Pregunta 1")
        question.answers.create(value=2, author=self.user_1)
        question.answers.create(value=2, author=self.user_2)
        #import pdb
        #pdb.set_trace()
        self.assertEqual(question.ranking(), 30)

    def test_answer_likes(self):
        question = Question.objects.get(description="Pregunta 2")
        question.answers.create(value=5, author=self.user_2, like=True)
        answer = Answer.objects.get(question=question, author=self.user_2)
        self.assertEqual(answer.like, True)

    def test_answer_dislikes(self):
        question = Question.objects.get(description="Pregunta 2")
        question.answers.create(value=1, author=self.user_2, like=False)
        answer = Answer.objects.get(question=question, author=self.user_2)
        self.assertEqual(answer.like, False)

    def test_answer_likes_ranking(self):
        question = Question.objects.get(description="Pregunta 1")
        question.answers.create(value=2, author=self.user_1, like=True)
        question.answers.create(value=2, author=self.user_2)
        self.assertEqual(question.ranking(), 35)

    def test_answer_dislikes_ranking(self):
        question = Question.objects.get(description="Pregunta 1")
        question.answers.create(value=2, author=self.user_1, like=True)
        question.answers.create(value=2, author=self.user_2, like=False)
        self.assertEqual(question.ranking(), 32)

