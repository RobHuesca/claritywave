# Base
pytz==2018.7
python-slugify==1.2.6
Pillow==5.3.0
psycopg2==2.7.4 --no-binary psycopg2

# Django
Django==3.2.5

# Environment
django-environ==0.4.5
